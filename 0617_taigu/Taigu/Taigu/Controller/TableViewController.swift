//
//  ViewController.swift
//  Taigu
//
//  Created by student on 2021/6/17.
//

import UIKit

class TableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var posts = [Post]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
       
        navigationController?.navigationBar.tintColor = Information.color
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor:Information.color]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:Information.color]
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.prefersLargeTitles = true
        
        posts = Information.posts
        
        AppDelegate.ob.subscribe { Post in
            self.tableView.reloadData()
        }
        
        
    }
    
//    func fetchPostData(completionHandler :@escaping ([Post]) -> Void){
//        let url = URL(string: "https://data.boch.gov.tw/data/opendata/v2/assetsCase/6.1.json")!
//        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
//
//            if error != nil {
//                print(error?.localizedDescription)
//                return
//            }
//            guard let data = data else{
//                return
//            }
//            do{
//                let postsData = try JSONDecoder().decode([Post].self, from: data)
//                completionHandler(postsData)
//            } catch{
//                print(error)
//                return
//            }
//            DispatchQueue.main.async{
//                self.tableView.reloadData()
//            }
//        }
//        task.resume()
//
//
//    }

    //MARK: - TableView
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = Information.cities[section]
        label.textColor = #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)
        label.backgroundColor = Information.color

        return label
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Information.cities.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cityName = Information.cities[section]
        if let posts = Information.classifyByCity[cityName] {
            let count = posts.count
            return count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
        let cityName = Information.cities[indexPath.section]
        if  let posts = Information.classifyByCity[cityName]{
            cell.textLabel?.text = posts[indexPath.row].caseName
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Segue
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goImageView" {
           if let imageVC = segue.destination as? ImageTableViewController,
              let indexPath = tableView.indexPathForSelectedRow{
            
//            imageVC.currentPost = posts[indexPath.row]
            let cityName = Information.cities[indexPath.section]
            if let posts = Information.classifyByCity[cityName]{
                imageVC.currentPost = posts[indexPath.row]
            }
            
        
            
           }
    }
    }
    


}

