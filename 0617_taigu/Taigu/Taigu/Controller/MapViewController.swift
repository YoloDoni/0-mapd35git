//
//  MapViewController.swift
//  Taigu
//
//  Created by student on 2021/6/17.
//

import UIKit
import MapKit
import CoreLocation
import RxSwift

class MapViewController: UIViewController ,CLLocationManagerDelegate{
    
    //主要掌管個人定位
    let manager = CLLocationManager()
    var annotationsArray = [MKPointAnnotation()]
    let bag = DisposeBag()
    let dispatchGroup = DispatchGroup()

    var userLocation : CLLocation?
   
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        setupUserTrackingButtonAndScaleView()
        
        let aleartController = UIAlertController(title: "資料讀取中", message: "地圖資料下載需要一些時間，請保持網路暢通。", preferredStyle: .alert)
        let action = UIAlertAction(title: "確認", style: .default, handler: nil)
        aleartController.addAction(action)
        present(aleartController, animated: true, completion: nil)
        
        mapView.showsScale = true
        //        setMapview()
        
        navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.tintColor = Information.color
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = Information.color
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor:Information.color]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:Information.color]
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        mapView.showsUserLocation = true
        mapView.delegate = self
        
        
        //        setupUserTrackingButtonAndScaleView()
        
        AppDelegate.ob.subscribe { String in
            
//            guard let cityPosts = Information.classifyByCity["臺北市"] else {
//                return
//            }
            
            let posts = Information.posts
            self.configure(posts:posts)
            //            self.mapView.delegate = self
            //                self.mapView.addAnnotations(self.annotationsArray)
        } onError: { Error in
            print(Error)
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: bag)
    }
    
    @IBAction func userLocationBtnPressed(_ sender: Any) {
        
        if let userlocation = userLocation{
            setRegion(userlocation)
        }
        
    }
    

    
    // MARK: - CLLocation
    //每當地點有更新的時候都會調用這個
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            manager.startUpdatingLocation()
            userLocation = location
            
        }
        
    }
    
    func setRegion(_ location:CLLocation){
        //指派的地點
        let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //縮小的範圍
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        //啟動region
        mapView.setRegion(region, animated: true)
    }
    

    
    
    //MARK: - appcoda
    func configure(posts:[Post]){
        for i in posts{
            dispatchGroup.enter()
            
        }
        geoAddress(posts: posts, i: 0)
        dispatchGroup.notify(queue: .main) {
            self.mapView.addAnnotations(self.annotationsArray)
            self.mapView.showAnnotations(self.annotationsArray, animated: true)
            
            let alertController = UIAlertController(title: "地圖標示已準備好", message: nil, preferredStyle: .alert)
            let alerAction = UIAlertAction(title: "確認", style: .default, handler: nil)
            alertController.addAction(alerAction)
            self.present(alertController, animated: true, completion: nil)
            
            print("Map done")
            if let userLocation =  self.userLocation  {
                self.setRegion(userLocation)
            }
            
//            if let index = Information.posts.firstIndex(where: {$0.caseName == "卑南文化岩雕石梯"}){
//                print("--------------------\(Information.posts[index])")
//            }else{
//                print("---------nofind")
//            }
            
            
        }
    }
    
    func geoAddress(posts:[Post], i : Int) {
        
        if i == posts.count {
            return
        }
        if let location = posts[i].keepPlaces[0].address{
            
            let addressArray = Array(Information.address.keys)
            
            //判斷地址有沒有重複
            
            guard addressArray.contains(location) == false else{
                let annotation = MyAnnotation(posts[i])
                annotation.title = posts[i].caseName
                annotation.subtitle = posts[i].caseId
                annotation.coordinate = Information.address[location]!
                self.annotationsArray.append(annotation)
                self.dispatchGroup.leave()
                self.geoAddress(posts: posts, i: i+1)
                self.annotationsArray.append(annotation)
                return
            }
            
            
            let geoCoder = CLGeocoder()
//            print("location\(location),i=\(i)")
            geoCoder.geocodeAddressString(location) { (placemarks, error) in
//                                    Thread.sleep(forTimeInterval: 0.7)
                if let error = error{
                    Information.address[location] = nil
                    self.dispatchGroup.leave()
//                    print(error.localizedDescription)
                    self.geoAddress(posts: posts, i: i+1)
                    return
                }
                
                if let placemarks = placemarks{
                    let placemark = placemarks[0]
                    let annotation = MyAnnotation(posts[i])
                    annotation.title = posts[i].caseName
                    annotation.subtitle = posts[i].caseId
                    if let location = placemark.location{
                        annotation.coordinate = location.coordinate
                        
                        //self.mapView.addAnnotation(annotation)
                    }
                    Information.address[location] = annotation.coordinate
                    self.annotationsArray.append(annotation)
                }
                self.dispatchGroup.leave()
                self.geoAddress(posts: posts, i: i+1)
            }
            
        }else{
            self.dispatchGroup.leave()
            self.geoAddress(posts: posts, i: i+1)
        }
        self.mapView.addAnnotations(self.annotationsArray)
       

        
    }
    
    
}


extension MapViewController:MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyMarker"
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        
        // Reuse the annotation if possible
        
        var annotationView:MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
        
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
        }
        annotationView?.canShowCallout = true
        annotationView?.isDraggable = true
        annotationView?.markerTintColor = Information.color
//        annotationView?.clusteringIdentifier = identifier
        annotationView?.showsLargeContentViewer = true
        
        let rightButton: AnyObject! = UIButton(type: UIButton.ButtonType.detailDisclosure)
        annotationView?.rightCalloutAccessoryView = rightButton as? UIView
        
        
        return annotationView
    }
    
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView{
            performSegue(withIdentifier: "mapToImage", sender: view)
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "mapToImage"{
            let nextVc = segue.destination as! ImageTableViewController
            let annotation = (sender as! MKAnnotationView).annotation
            let id = annotation?.subtitle
            
            if let index = Information.posts.firstIndex(where: { $0.caseId == id}){
                let post = Information.posts[index]
                nextVc.currentPost = post
                nextVc.targetLocation = annotation?.coordinate
            }

        }
    }
    
}


