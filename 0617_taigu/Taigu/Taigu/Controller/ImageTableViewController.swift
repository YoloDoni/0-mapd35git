//
//  ImageTableViewController.swift
//  Taigu
//
//  Created by student on 2021/6/17.
//

import UIKit
import RxSwift
import MapKit
import RealmSwift



class ImageTableViewController: UITableViewController{

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!

    

    var currentPost : Post?
    let bag = DisposeBag()
    let ob = ReplaySubject<[Post]>.create(bufferSize: 1)
    var targetLocation : CLLocationCoordinate2D?
    
    
   
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(currentPost?.caseId)
        
        navigationController?.navigationBar.tintColor = Information.color
        navigationController?.navigationBar.prefersLargeTitles = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 95.0
        tableView.rowHeight = UITableView.automaticDimension
        mapView.delegate = self
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        

        if let representImage = currentPost?.representImage{
            let set = CharacterSet.urlQueryAllowed
            let representImageString  = representImage.addingPercentEncoding(withAllowedCharacters: set)!
            fetchImage(url: representImageString)
        }else{
            
            imageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 0)
        }
        
        let annotation = MKPointAnnotation()
        annotation.title = currentPost?.caseName
        
        if let targetLocation = self.targetLocation{
            annotation.coordinate = targetLocation
            self.mapView.addAnnotation(annotation)
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: targetLocation, span: span)
            self.mapView.setRegion(region, animated: true)
        }else{
            if let location = currentPost?.keepPlaces[0].address{
    //            print(location)
                CLGeocoder().geocodeAddressString(location) { placemarks, error in
                    if let error = error{
                        return
                    }
                    if let placemarks = placemarks{
                        let placemark = placemarks[0]
                        if let location = placemark.location?.coordinate{
                            self.targetLocation = location
                            annotation.coordinate = location
                            self.mapView.addAnnotation(annotation)
                            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                            let region = MKCoordinateRegion(center: location, span: span)
                            self.mapView.setRegion(region, animated: true)

                        }else{
                            self.mapView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 0)
                        }
                    }
                }
            }

        }

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
//        print("dateFormatString: \(dateFormatString)")
        
        currentPost?.date = dateFormatString
        
//        let realm = try! Realm()
        let realm = RealmStore.shared.realm

        let posts = realm.objects(RealmPost.self)
        guard let caseId = currentPost?.caseId else{
            return
        }
        
//        do {
//            var realmPost = RealmPost()
//            realmPost.caseId = currentPost?.caseId
//            realmPost.caseName = currentPost?.caseName
//            realmPost.date = currentPost?.date
//
//            try realm.write({
//
//                realm.add(realmPost)
//
//            })
//        } catch  {
//            print("Realm沒查詢到然後錯誤")
//            print(LocalizedError.self)
//            assertionFailure()
//        }
        
        if let post =  posts.filter(" caseId = %@" , caseId).first{
            
            do {
                try realm.write({
                    post.date = dateFormatString
                    
                })
            } catch  {
                print("Realm: 有查詢到 然後錯誤")
                print(LocalizedError.self)
                assertionFailure()
            }
        }else{
            var realmPost = RealmPost()
            realmPost.caseId = currentPost?.caseId
            realmPost.caseName = currentPost?.caseName
            realmPost.date = currentPost?.date
            do {
                
                try realm.write({
                    realm.add(realmPost)
                })
            } catch  {
                print("Realm沒查詢到然後錯誤")
                print(LocalizedError.self)
                assertionFailure()
            }
            
        }
        

        

        
        

// MARK: Version of noramal historyPosts
//        if Information.historyPosts.contains(currentPost!){
//            let index = Information.historyPosts.firstIndex(of: currentPost!)
//            Information.historyPosts.remove(at: index!)
//            Information.historyPosts.insert(currentPost!, at: 0)
//        }else{
//            Information.historyPosts.insert(currentPost!, at: 0)
//        }
//
////
////
////        NotificationCenter.default.post(name: Notification.Name("abv"), object: nil, userInfo: ["posts":historyPosts])
//
      
    }
    

    // MARK: - Table view data source
    


    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//        cell.textLabel?.text = currentPost?.reserveStatusDesc
//
//        return cell
        switch indexPath.row
        {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ImageTableViewCell
            cell.ImageLabel.tintColor = Information.color
            cell.nameLabel.text = currentPost?.caseName
            cell.addressLabel.text = currentPost?.keepPlaces.first?.address
            cell.reasonLabel.text = currentPost?.registerReason
            cell.descriptionLabel.text = currentPost?.reserveStatusDesc
            cell.button.layer.cornerRadius = 5.0
            cell.button.backgroundColor = Information.color
            cell.button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
            return cell
        default:
            fatalError("i ma hee")
        }
        

    }
    
    @objc func buttonPressed(sender:UIButton){
        goMap(tagetlocation: targetLocation)
    }
  
    
    
    //MARK: - IMAGE GET
    
    func fetchImage (url:String){

        if let url = URL(string: url){
            let request = URLRequest(url: url)
            let session = URLSession.shared
            let task =  session.dataTask(with: request) { (data, response, error) in
                // 下載有結果 不一定是成功 所有參數都是optional
                if let error = error {
                    print(error)
                    return
                }
                guard let responseData = data else {
                    return
                }
                
                DispatchQueue.main.async{
                    let image = UIImage(data:responseData)
                    self.imageView.image = image
                }
            }
            task.resume()
        }
    }
    
    func downloadImagebyRx(url:String ) -> Observable<UIImage>{
        return Observable<UIImage>.create { subscriber in
           
            DispatchQueue.global().async{
                
                guard let url = URL(string: url)else{
                    return
                }
                do{
                    let data = try Data(contentsOf: url)
                    if let image = UIImage(data: data){
                        subscriber.onNext(image)
                        subscriber.onCompleted()
                    }
                }catch{
                    subscriber.onError(error)
                }
            }
            return Disposables.create()
        }
    }

}

//    MARK: -  MAP
    extension ImageTableViewController:MKMapViewDelegate{
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            let identifier = "MyMarker"
            
            if annotation.isKind(of: MKUserLocation.self) {
                return nil
            }
            
            // Reuse the annotation if possible
            
            var annotationView:MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
            
            if annotationView == nil {
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                
            }
//            annotationView?.canShowCallout = true
//            annotationView?.isDraggable = true
 //        annotationView?.clusteringIdentifier = identifier
//            annotationView?.showsLargeContentViewer = true

            annotationView?.markerTintColor = Information.color

            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            goMap(tagetlocation: targetLocation)
        }
        
        func goMap(tagetlocation :CLLocationCoordinate2D?){
            if let targetLocation = targetLocation {
                //透過地targetLocation建立一個MKMapItem
                let targetPlacemark=MKPlacemark(coordinate: targetLocation)
                // 目標地圖項目
                let targetItem=MKMapItem(placemark: targetPlacemark)
                let userMapItem=MKMapItem.forCurrentLocation()
                let routes=[userMapItem,targetItem]
                MKMapItem.openMaps(with: routes, launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
            }else{
                let aleartController = UIAlertController(title: "資料讀取中", message: "資料下載過於繁忙，請稍後再試，或是暫無有效地址資訊。", preferredStyle: .alert)
                let action = UIAlertAction(title: "確認", style: .default, handler: nil)
                aleartController.addAction(action)
                present(aleartController, animated: true, completion: nil)
                
                
            }

            
        }
        
    }
    



