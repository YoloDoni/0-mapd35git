//
//  InterduceViewController.swift
//  Taigu
//
//  Created by student on 2021/6/17.
//

import UIKit
import RealmSwift
import RxSwift

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    @IBOutlet weak var tableView: UITableView!
    let bag = DisposeBag()
    let realm = RealmStore.shared.realm.objects(RealmPost.self).sorted(byKeyPath: "date", ascending: false)

    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = Information.color
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor:Information.color]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:Information.color]
        
        tableView.delegate = self
        tableView.dataSource = self
//        NotificationCenter.default.addObserver(self, selector: #selector(aa), name: Notification.Name("abv"), object: nil)
//
//        guard let ImageTableViewController = storyboard?.instantiateViewController(withIdentifier: "ImageTableViewController") as? ImageTableViewController else{
//            return
//            }
////        historyPosts = ImageTableViewController.historyPosts
//
//        ImageTableViewController.ob.subscribe { posts in
//            self.historyPosts = posts
//        } onError: { Error in
//            print(Error)
//        } onCompleted: {
//
//        } onDisposed: {
//
//        }.disposed(by: bag)

        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        return realm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = realm[indexPath.row].caseName
        cell.detailTextLabel?.text = realm[indexPath.row].date
        cell.textLabel?.font = UIFont(name:"Avenir", size:16)
        cell.detailTextLabel?.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        return cell

    }
    
//  MARK: Version of noramal historyPosts
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return Information.historyPosts.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//
//
//        cell.textLabel?.text = Information.historyPosts[indexPath.row].caseName
//        cell.detailTextLabel?.text = Information.historyPosts[indexPath.row].date
//        cell.textLabel?.font = UIFont(name:"Avenir", size:16)
//        cell.detailTextLabel?.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
//
//
//
//
//        return cell
//
//    }
//
//    @objc func aa (_ notification:Notification){
//        historyPosts =  (notification.userInfo!["posts"] as? [Post])!
//
//    }
//
    
    
    //MARK: - Segue
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "historyToImage" {
            if let imageVC = segue.destination as? ImageTableViewController,
               let indexPath = tableView.indexPathForSelectedRow{
                guard let index = Information.posts.firstIndex(where: { post  in
                    
                    post.caseId == realm[indexPath.row].caseId
                }) else {
                    return
                }
                
                let post = Information.posts[index]
                imageVC.currentPost = post
                
            }
        }
    }



}
