//
//  Post.swift
//  Taigu
//
//  Created by student on 2021/6/17.
//

import Foundation
import RealmSwift
import MapKit



class Post :  Codable{
    static func == (lhs: Post, rhs: Post) -> Bool {
        return lhs.caseId == rhs.caseId
    }
    
    var caseId : String
    var caseName :String
    var registerReason :String
    var keepPlaces : [KeepPlaces]
    var representImage : String?
    var descAge : String?
    var reserveStatusDesc: String?
    var govInstitutionName : String?
    var date : String?
    
    
    
}



class KeepPlaces:Codable {
    var address : String?
}
