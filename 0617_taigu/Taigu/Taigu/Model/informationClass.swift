//
//  informationClass.swift
//  Taigu
//
//  Created by student on 2021/7/5.
//

import Foundation
import UIKit
import MapKit
import RealmSwift
class Information {
    
    static let shared = Information()
    
    
    static let color = UIColor(red: 231/255, green: 148/255, blue: 96/255, alpha: 1)
    static var posts:[Post] = []
    static var historyPosts = [Post]()
    static var cities = [String]()
    static var classifyByCity = [String:[Post]]()
    static var address = [String:CLLocationCoordinate2D]()
    
    private init(){
        
        
    }
    
    func fetchPostData(completionHandler :(([Post]) -> Void)? = nil){
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        let url = URL(string: "https://data.boch.gov.tw/data/opendata/v2/assetsCase/6.1.json")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print("fetchpostdata\(error?.localizedDescription)")
                dispatchGroup.leave()
                return
            }
            guard let data = data else{
                dispatchGroup.leave()
                return
            }
            do{
                let postsData = try JSONDecoder().decode([Post].self, from: data)
//                completionHandler(postsData)
                Information.posts = postsData
                Information.shared.findCity(posts: Information.posts)

            } catch{
                print("fetchpostdata2\(error)")
            }
            dispatchGroup.leave()
            print("downloadDone")
        }

        task.resume()
        dispatchGroup.notify(queue: .main) {
            AppDelegate.ob.onNext("dondonedone")
            
        }
    }
    

    
    func findCity(posts:[Post])  {



        for post in posts{

            guard let citySubstring = post.govInstitutionName?.prefix(3) else{
                continue
            }
            let city = String(citySubstring)

            if Information.cities.contains(city) {
                //如果裡面有的話
                //字典[城市]?.append(post)
                Information.classifyByCity[city]!.append(post)
//                continue
            }
            else  {
//                裡面沒有的話 cities就append
//                字典[城市].append(post)
                Information.cities.append(city)
                Information.classifyByCity[city] = [post]

            }

        }
        Information.cities = Information.cities.sorted()
//        Information.classifyByCity = Information.classifyByCity.keys.sorted()
        
    }

}
