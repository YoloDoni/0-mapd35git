//
//  RealmPost.swift
//  Taigu
//
//  Created by student on 2021/7/30.
//


import Foundation
import RealmSwift


class RealmPost : Object{

    @objc dynamic var caseId : String?
    @objc dynamic var caseName :String?
    @objc dynamic var registerReason :String?
    @objc dynamic var address : String?
    @objc dynamic var representImage : String?
    @objc dynamic  var descAge : String?
    @objc dynamic var reserveStatusDesc: String?
    @objc dynamic  var govInstitutionName : String?
    @objc dynamic var date : String?
    
    
}



