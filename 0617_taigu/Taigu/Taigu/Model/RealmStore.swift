//
//  RealmStore.swift
//  Taigu
//
//  Created by student on 2021/7/29.
//

import Foundation
import RealmSwift
 
class RealmStore {
    
    static let shared =  RealmStore()
    var realm = try! Realm()
    
    private init() {
        
    }
    
}
